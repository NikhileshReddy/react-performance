import React from 'react';
import axios from 'axios';
import './image.css';
// function ImagesComponent (){
//  return <h1>Images Component</h1>
// }

class ImagesComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
      loading: true,
      error: null
    };
  }

  componentDidMount(page) {
    // let page =1
    // Remove the 'www.' to cause a CORS error (and see the error state)
    axios.get(`https://jsonplaceholder.typicode.com/photos`)
      .then(res => {
        // Transform the raw data by extracting the nested posts
        const posts = res.data;
     
        // console.log(posts);
        // Update state to trigger a re-render.
        // Clear any errors, and turn off the loading indiciator.
        this.setState({
          posts,
          loading: false,
          error: null
        });
      })
      .catch(err => {
        // Something went wrong. Save the error in state and re-render.
        this.setState({
          loading: false,
          error: err
        });
      });
  }

  renderLoading() {
    return <div></div>;
  }

  renderError() {
    return (
      <div>
        Uh oh: {this.state.error.message}
      </div>
    );
  }

  renderPosts() {
    if(this.state.error) {
      return this.renderError();
    }

    return (
      <div>
        {this.state.posts.map(post =>
          <div  key={post.id}><img alt="img" src={post.name}></img></div>
        )}
      </div>
    // <img src="{posts.thumbnailUrl}" alt=""></img>
    );
  }

  render() {
    return (
      <div>
        {/* <h1>{`/r/${this.props.subreddit}`}</h1> */}
        {this.state.loading ?
          this.renderLoading()
          : this.renderPosts()}
      </div>
    );
  }
}


export default ImagesComponent;