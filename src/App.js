import React, {Component} from 'react';
// import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  // Link
} from 'react-router-dom'

// import HomeComponent from './home';
import ImagesComponent from './images';
import UsersComponent from './users';
import HeaderComponent from './headerComponent';


class App extends Component {
    render() {
    return (
      <Router>
        <div className="App">
        <HeaderComponent></HeaderComponent>
              {/* <Route exact path='/' component={HomeComponent}></Route> */}
              <Route exact path='/images' component={ImagesComponent}></Route>
              <Route exact path='/' component={UsersComponent}></Route>
        </div>
      </Router>
    );
  }
}

export default App;
