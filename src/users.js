import React from "react";
import axios from "axios";
import "./user.css";
// function UsersComponent (){
//  return <h1>Users Component</h1>
// }
class UsersComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
      loading: true,
      error: null
    };
  }

  componentDidMount() {
    // Remove the 'www.' to cause a CORS error (and see the error state)
    axios
      .get(`https://jsonplaceholder.typicode.com/users`)
      .then(res => {
        // Transform the raw data by extracting the nested posts
        const posts = res.data;

        // console.log(posts);
        // Update state to trigger a re-render.
        // Clear any errors, and turn off the loading indiciator.
        this.setState({
          posts,
          loading: false,
          error: null
        });
      })
      .catch(err => {
        // Something went wrong. Save the error in state and re-render.
        this.setState({
          loading: false,
          error: err
        });
      });
  }

  renderLoading() {
    return <div />;
  }

  renderError() {
    return <div>Uh oh: {this.state.error.message}</div>;
  }

  renderPosts() {
    if (this.state.error) {
      return this.renderError();
    }

    return (
      <div className="row m-0 p-0">
        {this.state.posts.map(post => (
          
            <div key={post.id} className="col-md-3">
              <div className="card m-2 shadow-lg rounded border-0 w-100">
                <i className="fas fa-user card-img-top fa-5x text-center mt-3 text-primary" />
                <div className="card-body">
                  <h5 className="card-title text-center">{post.name}</h5>
                  <iframe
                    title="usermap"
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7776.019318289843!2d77.61794623482311!3d12.971233565935798!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae168370e795bd%3A0x36c478eeccc87d29!2sTrinity!5e0!3m2!1sen!2sin!4v1556110379252!5m2!1sen!2sin"
                    frameBorder="0"
                    className="col-md-12 p-0 m-0"
                  />
                  <small className="card-text m-2 row">
                    {post.address.street},{post.address.suite},
                    {post.address.city},{post.address.zipcode}.
                  </small>
                  <a href="/" className="btn btn-primary col">
                    {post.email}
                  </a>
                </div>
              </div>
            </div>
          
        ))}
      </div>
      // <img src="{posts.thumbnailUrl}" alt=""></img>
    );
  }

  render() {
    return (
      <div>
        {/* <h1>{`/r/${this.props.subreddit}`}</h1> */}
        {this.state.loading ? this.renderLoading() : this.renderPosts()}
      </div>
    );
  }
}

export default UsersComponent;
