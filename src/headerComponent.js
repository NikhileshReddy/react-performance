import React from "react";
import { Link } from "react-router-dom";
import "./headerComponent.css";

function HeaderComponent() {
  return (
    <div className="row pl-5 mb-3 m-0">
      <img
        src={process.env.PUBLIC_URL + 'logo.png'}
        alt="logo"
        className="ml-auto logo"
      />
      <Link
        to="/users"
        className="ml-auto text-center  mr-2 mt-3"
      >
        <i className="fas fa-users fa-2x text-center text-primary" /> <br />
        Users
      </Link>
      <Link
        to="/images"
        className="mr-5 text-center ml-2 mt-3"
      >
        <i className="fas fa-images fa-2x text-center text-primary" />
        <br />
        Images
      </Link>
    </div>
  );
}

export default HeaderComponent;
